package org.twbillot.ArrayUtils;

public final class ArrayUtils extends ArrayCommonUtils {
	// AddAt methods
	public static	<T>	T[]	addAt(T[] arr, T val, int idx, boolean safe) {
		if (safe) { return safeAddAt(arr, val, idx); } return unsafeAddAt(arr, val, idx);
	}
	public static	<T>	T[]	addAt(T[] arr, T[] vals, int idx, boolean safe) {
		if (safe) { return safeAddAt(arr, vals, idx); } return unsafeAddAt(arr, vals, idx);
	}
	public static	<T>	T[]	addAt(T[] arr, T val, int idx) {
		return addAt(arr, val, idx, false);
	}
	public static	<T>	T[]	addAt(T[] arr, T[] vals, int idx) {
		return addAt(arr, vals, idx, false);
	}
	public static	<T>	T[]	safeAddAt(T[] arr, T val, int idx) {
		return ArraySafeUtils.addAt(arr, val, idx);
	}
	public static	<T>	T[]	safeAddAt(T[] arr, T[] vals, int idx) {
		return ArraySafeUtils.addAt(arr, vals, idx);
	}
	public static	<T>	T[]	unsafeAddAt(T[] arr, T val, int idx) {
		return ArrayUnsafeUtils.addAt(arr, val, idx);
	}
	public static	<T>	T[]	unsafeAddAt(T[] arr, T[] vals, int idx) {
		return ArrayUnsafeUtils.addAt(arr, vals, idx);
	}
	// RemoveAt methods
	public static	<T>	T[]	removeAt(T[] arr, int idx, boolean safe) {
		if (safe) { return safeRemoveAt(arr, idx); } return unsafeRemoveAt(arr, idx);
	}
	public static	<T>	T[]	removeAt(T[] arr, int idx, int nbr, boolean safe) {
		if (safe) { return safeRemoveAt(arr, idx, nbr); } return unsafeRemoveAt(arr, idx, nbr);
	}
	public static	<T>	T[]	removeAt(T[] arr, int idx) {
		return removeAt(arr, idx, false);
	}
	public static	<T>	T[]	removeAt(T[] arr, int idx, int nbr) {
		return removeAt(arr, idx, nbr, false);
	}
	public static	<T>	T[]	safeRemoveAt(T[] arr, int idx) {
		return ArraySafeUtils.removeAt(arr, idx);
	}
	public static	<T>	T[]	safeRemoveAt(T[] arr, int idx, int nbr) {
		return ArraySafeUtils.removeAt(arr, idx, nbr);
	}
	public static	<T>	T[]	unsafeRemoveAt(T[] arr, int idx) {
		return ArrayUnsafeUtils.removeAt(arr, idx);
	}
	public static	<T>	T[]	unsafeRemoveAt(T[] arr, int idx, int nbr) {
		return ArrayUnsafeUtils.removeAt(arr, idx, nbr);
	}
	// Shift methods
	public static	<T>	T[]	shift(T[] arr, T val, boolean safe) {
		if (safe) { return safeShift(arr, val); } return unsafeShift(arr, val);
	}
	public static	<T>	T[]	shift(T[] arr, T[] vals, boolean safe) {
		if (safe) { return safeShift(arr, vals); } return unsafeShift(arr, vals);
	}
	public static	<T>	T[]	shift(T[] arr, T val) {
		return shift(arr, val, false);
	}
	public static	<T>	T[]	shift(T[] arr, T[] vals) {
		return shift(arr, vals, false);
	}
	public static	<T>	T[]	safeShift(T[] arr, T val) {
		return ArraySafeUtils.shift(arr, val);
	}
	public static	<T>	T[]	safeShift(T[] arr, T[] vals) {
		return ArraySafeUtils.shift(arr, vals);
	}
	public static	<T>	T[]	unsafeShift(T[] arr, T val) {
		return ArrayUnsafeUtils.shift(arr, val);
	}
	public static	<T>	T[]	unsafeShift(T[] arr, T[] vals) {
		return ArrayUnsafeUtils.shift(arr, vals);
	}
	// Unshift methods
	public static	<T>	T[]	unshift(T[] arr, boolean safe) {
		if (safe) { return safeUnshift(arr); } return unsafeUnshift(arr);
	}
	public static	<T>	T[]	unshift(T[] arr, int nbr, boolean safe) {
		if (safe) { return safeUnshift(arr, nbr); } return unsafeUnshift(arr, nbr);
	}
	public static	<T>	T[]	unshift(T[] arr) {
		return unshift(arr, false);
	}
	public static	<T>	T[]	unshift(T[] arr, int nbr) {
		return unshift(arr, nbr, false);
	}
	public static	<T>	T[]	safeUnshift(T[] arr) {
		return ArraySafeUtils.unshift(arr);
	}
	public static	<T>	T[]	safeUnshift(T[] arr, int nbr) {
		return ArraySafeUtils.unshift(arr, nbr);
	}
	public static	<T>	T[]	unsafeUnshift(T[] arr) {
		return ArrayUnsafeUtils.unshift(arr);
	}
	public static	<T>	T[]	unsafeUnshift(T[] arr, int nbr) {
		return ArrayUnsafeUtils.unshift(arr, nbr);
	}
	// Trunc methods
	public static	<T>	T[]	trunc(T[] arr, boolean safe) {
		if (safe) { return safeTrunc(arr); } return unsafeTrunc(arr);
	}
	public static	<T>	T[]	trunc(T[] arr, int nbr, boolean safe) {
		if (safe) { return safeTrunc(arr, nbr); } return unsafeTrunc(arr, nbr);
	}
	public static	<T>	T[]	trunc(T[] arr) {
		return trunc(arr, false);
	}
	public static	<T>	T[]	trunc(T[] arr, int nbr) {
		return trunc(arr, nbr, false);
	}
	public static	<T>	T[]	safeTrunc(T[] arr) {
		return ArraySafeUtils.trunc(arr);
	}
	public static	<T>	T[]	safeTrunc(T[] arr, int nbr) {
		return ArraySafeUtils.trunc(arr, nbr);
	}
	public static	<T>	T[]	unsafeTrunc(T[] arr) {
		return ArrayUnsafeUtils.trunc(arr);
	}
	public static	<T>	T[]	unsafeTrunc(T[] arr, int nbr) {
		return ArrayUnsafeUtils.trunc(arr, nbr);
	}
	// RemoveFrom methods
	public static	<T>	T[]	removeFrom(T[] arr, int from, boolean safe) {
		if (safe) { return safeRemoveFrom(arr, from); } return unsafeRemoveFrom(arr, from);
	}
	public static	<T>	T[]	removeFrom(T[] arr, int from) {
		return removeFrom(arr, from, false);
	}
	public static	<T>	T[]	safeRemoveFrom(T[] arr, int from) {
		return ArraySafeUtils.removeFrom(arr, from);
	}
	public static	<T>	T[]	unsafeRemoveFrom(T[] arr, int from) {
		return ArrayUnsafeUtils.removeFrom(arr, from);
	}
	// RemoveTo methods
	public static	<T>	T[]	removeTo(T[] arr, int to, boolean safe) {
		if (safe) { return safeRemoveTo(arr, to); } return unsafeRemoveTo(arr, to);
	}
	public static	<T>	T[]	removeTo(T[] arr, int to) {
		return removeTo(arr, to, false);
	}
	public static	<T>	T[]	safeRemoveTo(T[] arr, int to) {
		return ArraySafeUtils.removeTo(arr, to);
	}
	public static	<T>	T[]	unsafeRemoveTo(T[] arr, int to) {
		return ArrayUnsafeUtils.removeTo(arr, to);
	}
	// Slice methods
	public static	<T>	T[]	slice(T[] arr, int idx, int nbr, boolean safe) {
		if (safe) { return safeSlice(arr, idx, nbr); } return unsafeSlice(arr, idx, nbr);
	}
	public static	<T>	T[]	slice(T[] arr, int idx, boolean safe) {
		if (safe) { return safeSlice(arr, idx); } return unsafeSlice(arr, idx);
	}
	public static	<T>	T[]	slice(T[] arr, int idx, int nbr) {
		return slice(arr, idx, nbr, false);
	}
	public static	<T>	T[]	slice(T[] arr, int idx) {
		return slice(arr, idx, false);
	}
	public static	<T>	T[]	safeSlice(T[] arr, int idx, int nbr) {
		return ArraySafeUtils.slice(arr, idx, nbr);
	}
	public static	<T>	T[]	safeSlice(T[] arr, int idx) {
		return ArraySafeUtils.slice(arr, idx);
	}
	public static	<T>	T[]	unsafeSlice(T[] arr, int idx, int nbr) {
		return ArrayUnsafeUtils.slice(arr, idx, nbr);
	}
	public static	<T>	T[]	unsafeSlice(T[] arr, int idx) {
		return ArrayUnsafeUtils.slice(arr, idx);
	}
}