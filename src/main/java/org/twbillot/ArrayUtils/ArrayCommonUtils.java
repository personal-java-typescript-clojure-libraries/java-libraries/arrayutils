package org.twbillot.ArrayUtils;

import java.lang.reflect.Array;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

abstract class ArrayCommonUtils {
	/*
	TODO: Treatment of Primitive Types (byte, short, int, long, float, double, char, boolean)
	*/
	@SuppressWarnings("unchecked")
	protected static	<T>	T[]	newArray(
			int			len, 
			Class<T>	comp) {
		return (T[]) Array.newInstance(comp, len);
	}
	protected static	<T>	T[]	newArray(
			Class<T>	comp) {
		return newArray(0, comp);
	}
	@SuppressWarnings("unchecked")
	protected static	<T>	T[]	newArray(
			int	len, 
			T[]	arr) {
		return (T[]) newArray(len, arr.getClass().getComponentType());
	}
	protected static	<T>	T[]	newArray(
			T[]	arr) {
		return newArray(0, arr);
	}
	@SafeVarargs
	protected static	<T>	T[]	newArray(
			T[]		arr, 
			T...	vals) {
		return vals;
	}
	// Add methods
	public static	<T>	T[]	add(
			T[]	arr, 
			T	val) {
		return add(arr, newArray(arr, val));
	}
	public static	<T>	T[]	add(
			T[]	arr, 
			T[]	vals) {
		int arrLen = arr.length;
		int valLen = vals.length;
		T[] newArr = newArray(arrLen + valLen, arr);
		System.arraycopy(
				arr, 0, 
				newArr, 0, arrLen);
		System.arraycopy(
				vals, 0, 
				newArr, arrLen, valLen);
		return newArr;
	}
	// Remove methods
	public static	<T>	T[]	remove(
			T[]	arr, 
			T	val) {
		int idx = firstIndex(arr, val);
		int arrLen = arr.length;
		T[] newArr = newArray(arrLen - 1, arr);
		System.arraycopy(
				arr, 0, 
				newArr, 0, idx);
		System.arraycopy(
				arr, (idx + 1), 
				newArr, idx, arrLen - 1 - idx);
		return newArr;
	}
	public static	<T>	T[]	remove(
			T[]	arr, 
			T[]	vals) {
		T[] newArr = add(newArray(arr), arr);
		for (T val : vals) { newArr = remove(newArr, val); }
		return newArr;
	}
	// RemoveAll methods
	public static	<T>	T[]	removeAll(
			T[]	arr, 
			T	val) {
		return not(arr, val);
	}
	public static	<T>	T[]	removeAll(
			T[]	arr, 
			T[]	vals) {
		T[] newArr = add(newArray(arr), arr);
		for (T val : vals) { newArr = removeAll(newArr, val); }
		return newArr;
	}
	// Unique methods
	public static	<T>	T[]	unique(
			T[]	arr) {
		T[] newArr = newArray(arr);
		for (T i : arr) { if (! contain(newArr, i)) { newArr = add(newArr, i); } }
		return newArr;
	}
	// ForEach methods
	public static	<T>		T[]	forEach(
			T[]					arr, 
			Consumer<T> 		ope) {
		for (T i : arr) { ope.accept(i); }
		return arr;
	}
	public static	<T, U>	U[]	forEach(
			T[]					arrIn, 
			Class<U>			classOut, 
			Function<T, U>		ope) {
		int len = arrIn.length;
		U[] arrOut = newArray(len, classOut);
		for (int i = 0; i < len; i++) { arrOut[i] = ope.apply(arrIn[i]); }
		return arrOut;
	}
	public static	<T, U>	U	forEach(
			T[]					arrIn, 
			U					valOut, 
			BiFunction<T, U, U>	ope) {
		for (T i : arrIn) { valOut = ope.apply(i, valOut); }
		return valOut;
	}
	// Find methods
	public static	<T>		T[]	find(
			T[]							arrIn, 
			T							val) {
		T[] arrOut = newArray(arrIn);
		for (T i : arrIn) { if (val.equals(i)) { arrOut = add(arrOut, i); } }
		return arrOut;
	}
	public static	<T>		T[]	find(
			T[]							arrIn, 
			Function<T, Boolean>		ope) {
		T[] arrOut = newArray(arrIn);
		for (T i : arrIn) { if (ope.apply(i)) { arrOut = add(arrOut, i); } }
		return arrOut;
	}
	public static	<T, U>	T[]	find(
			T[]							arrIn, 
			U							argIn, 
			BiFunction<T, U, Boolean>	ope) {
		T[] arrOut = newArray(arrIn);
		for (T i : arrIn) { if (ope.apply(i, argIn)) { arrOut = add(arrOut, i); } }
		return arrOut;
	}
	// Not methods
	public static	<T>		T[]	not(
			T[]	arrIn, 
			T	val) {
		T[] arrOut = newArray(arrIn);
		for (T i : arrIn) { if (! val.equals(i)) { arrOut = add(arrOut, i); } }
		return arrOut;
	}
	// IndexOf methods
	public static	<T>		Integer[]	indexesOf(
			T[]							arr, 
			T							val) {
		Integer[] arrOut = new Integer[0];
		for (int i = 0; i < arr.length; i++) { if (val.equals(arr[i])) { arrOut = add(arrOut, i); } }
		return arrOut;
	}
	public static	<T>		Integer[]	indexesOf(
			T[]							arrIn, 
			Function<T, Boolean>		ope) {
		Integer[] arrOut = new Integer[0];
		for (int i = 0; i < arrIn.length; i++) { if (ope.apply(arrIn[i])) { arrOut = add(arrOut, i); } }
		return arrOut;
	}
	public static	<T, U>	Integer[]	indexesOf(
			T[]							arrIn, 
			U							argIn, 
			BiFunction<T, U, Boolean>	ope) {
		Integer[] arrOut = new Integer[0];
		for (int i = 0; i < arrIn.length; i++) { if (ope.apply(arrIn[i], argIn)) { arrOut = add(arrOut, i); } }
		return arrOut;
	}
	// Contain methods
	public static	<T>		Boolean	contain(
			T[]							arr, 
			T							val) {
		Boolean found = false;
		int i = 0;
		while (! found && i < arr.length) {
			found = arr[i].equals(val);
			i++;
		}
		return found;
	}
	public static	<T>		Boolean	contain(
			T[]							arr, 
			Function<T, Boolean>		ope) {
		Boolean found = false;
		int i = 0;
		while (! found && i < arr.length) {
			found = ope.apply(arr[i]);
			i++;
		}
		return found;
	}
	public static	<T, U>	Boolean	contain(
			T[]							arr, 
			U							argIn, 
			BiFunction<T, U, Boolean>	ope) {
		Boolean found = false;
		int i = 0;
		while (! found && i < arr.length) {
			found = ope.apply(arr[i], argIn);
			i++;
		}
		return found;
	}
	// First methods
	public static	<T>		T	first(
			T[]						arr, 
			T						val) {
		T valOut = null;
		int i = 0;
		while (valOut == null && i < arr.length) {
			T element = arr[i];
			if (element.equals(val)) { valOut = element; }
			i++;
		}
		return valOut;
	}
	public static	<T>		T	first(
			T[]						arr, 
			Function<T, Boolean>	ope) {
		T valOut = null;
		int i = 0;
		while (valOut == null && i < arr.length) {
			T element = arr[i];
			if (ope.apply(element)) { valOut = element; }
			i++;
		}
		return valOut;
	}
	public static	<T, U>	T	first(
			T[]							arr, 
			U							argIn, 
			BiFunction<T, U, Boolean>	ope) {
		T valOut = null;
		int i = 0;
		while (valOut == null && i < arr.length) {
			T element = arr[i];
			if (ope.apply(element, argIn)) { valOut = element; }
			i++;
		}
		return valOut;
	}
	// FirstIndex methods
	public static	<T>		Integer	firstIndex(
			T[]							arr, 
			T							val) {
		Integer valOut = null;
		int i = 0;
		while (valOut == null && i < arr.length) {
			if (arr[i].equals(val)) { valOut = i; }
			i++;
		}
		return valOut;
	}
	public static	<T>		Integer	firstIndex(
			T[]							arr, 
			Function<T, Boolean>		ope) {
		Integer valOut = null;
		int i = 0;
		while (valOut == null && i < arr.length) {
			if (ope.apply(arr[i])) { valOut = i; }
			i++;
		}
		return valOut;
	}
	public static	<T, U>	Integer	firstIndex(
			T[]							arr, 
			U							argIn, 
			BiFunction<T, U, Boolean>	ope) {
		Integer valOut = null;
		int i = 0;
		while (valOut == null && i < arr.length) {
			if (ope.apply(arr[i], argIn)) { valOut = i; }
			i++;
		}
		return valOut;
	}
}