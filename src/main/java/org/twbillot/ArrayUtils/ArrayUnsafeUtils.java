package org.twbillot.ArrayUtils;

public final class ArrayUnsafeUtils extends ArrayCommonUtils {
	private static	void	verifyIndex(
			int	idx, 
			int	len) 
			throws	ArrayIndexOutOfBoundsException {
		if (idx < 0) {
			throw new ArrayIndexOutOfBoundsException("Index < 0: " + idx);
		}
		if (len < idx) {
			throw new ArrayIndexOutOfBoundsException("Array size < Index: " + len + " < " + idx);
		}
	}
	private static	void	verifyNumber(
			int	nbr, 
			int	len) 
			throws	IllegalArgumentException, 
					NegativeArraySizeException {
		if (nbr < 0) { 
			throw new IllegalArgumentException("Number of items < 0: " + nbr);
		}
		if (len < nbr) {
			throw new NegativeArraySizeException("Array size < Number of items: " + len + " < " + nbr);
		}
	}
	// AddAt methods
	public static	<T>	T[]	addAt(
			T[]	arr, 
			T	val, 
			int	idx) {
		return addAt(arr, newArray(arr, val), idx);
	}
	public static	<T>	T[]	addAt(
			T[]	arr, 
			T[]	vals, 
			int	idx) {
		int arrLen = arr.length;
		int valLen = vals.length;
		T[] newArr = newArray((arrLen + valLen), arr);
		System.arraycopy(
				arr, 0, 
				newArr, 0, idx);
		System.arraycopy(
				vals, 0, 
				newArr, idx, valLen);
		System.arraycopy(
				arr, idx, 
				newArr, (idx + valLen), (arrLen - idx));
		return newArr;
	}
	// RemoveAt methods
	public static	<T>	T[]	removeAt(
			T[]	arr, 
			int	idx) {
		return removeAt(arr, idx, 1);
	}
	public static	<T>	T[]	removeAt(
			T[]	arr, 
			int	idx, 
			int	nbr) 
			throws	ArrayIndexOutOfBoundsException, 
					IllegalArgumentException, 
					NegativeArraySizeException {
		int arrLen = arr.length;
		verifyIndex(idx, arrLen);
		verifyNumber(nbr, arrLen);
		T[] newArr = newArray((arrLen - nbr), arr);
		System.arraycopy(
				arr, 0, 
				newArr, 0, idx);
		System.arraycopy(
				arr, (idx + nbr), 
				newArr, idx, (arrLen - nbr - idx));
		return newArr;
	}
	// Shift methods
	public static	<T>	T[]	shift(
			T[]	arr, 
			T	val) {
		return shift(arr, newArray(arr, val));
	}
	public static	<T>	T[]	shift(
			T[]	arr, 
			T[]	vals) {
		return addAt(arr, vals, 0);
	}
	// Unshift methods
	public static	<T>	T[]	unshift(
			T[]	arr) {
		return unshift(arr, 1);
	}
	public static	<T>	T[]	unshift(
			T[]	arr, 
			int	nbr) {
		return removeAt(arr, 0, nbr);
	}
	// Trunc methods
	public static	<T>	T[]	trunc(
			T[]	arr) {
		return trunc(arr, 1);
	}
	public static	<T>	T[]	trunc(
			T[]	arr, 
			int	nbr) {
		return removeAt(arr, (arr.length - nbr), nbr);
	}
	// RemoveFrom methods
	public static	<T>	T[]	removeFrom(
			T[]	arr, 
			int	from) {
		return removeAt(arr, from, (arr.length - from));
	}
	// RemoveTo methods
	public static	<T>	T[]	removeTo(
			T[]	arr, 
			int	to) {
		return removeAt(arr, 0, to);
	}
	// Slice methods
	public static	<T>	T[]	slice(
			T[]	arr, 
			int	idx, 
			int	nbr) 
			throws	ArrayIndexOutOfBoundsException, 
					IllegalArgumentException, 
					NegativeArraySizeException {
		int arrLen = arr.length;
		verifyIndex(idx, arrLen);
		verifyNumber(nbr, arrLen);
		T[] newArr = newArray(nbr, arr);
		System.arraycopy(
				arr, idx, 
				newArr, 0, nbr);
		return newArr;
	}
	public static	<T>	T[]	slice(
			T[]	arr, 
			int	idx) {
		return slice(arr, idx, 1);
	}
}