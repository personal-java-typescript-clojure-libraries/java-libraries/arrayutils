package org.twbillot.ArrayUtils;

public final class ArraySafeUtils extends ArrayCommonUtils {
	private static	int		limitAddIndex(
			int	idx, 
			int len) {
		if (idx < 0) { idx = len + idx; }
		if (idx < 0) { idx = 0; }
		if (len < idx) { idx = len; }
		return idx;
	}
	private static	int		limitRemoveIndex(
			int	idx, 
			int len) {
		if (idx < 0) { idx = len + idx; }
		if (idx < 0) { idx = 0; }
		if (len < idx) { idx = len - 1; }
		return idx;
	}
	private static	int		limitNumber(
			int	idx, 
			int	nbr, 
			int len) {
		if (nbr < 0) { nbr = 0; }
		if (len - idx <= nbr) { nbr = len - idx; }
		return nbr;
	}
	// AddAt methods
	public static	<T>	T[]	addAt(
			T[]	arr, 
			T	val, 
			int	idx) {
		return addAt(arr, newArray(arr, val), idx);
	}
	public static	<T>	T[]	addAt(
			T[]	arr, 
			T[]	vals, 
			int	idx) {
		int arrLen = arr.length;
		int valLen = vals.length;
		idx = limitAddIndex(idx, arrLen);
		T[] newArr = newArray((arrLen + valLen), arr);
		System.arraycopy(
				arr, 0, 
				newArr, 0, idx);
		System.arraycopy(
				vals, 0, 
				newArr, idx, valLen);
		System.arraycopy(
				arr, idx, 
				newArr, (idx + valLen), (arrLen - idx));
		return newArr;
	}
	// RemoveAt methods
	public static	<T>	T[]	removeAt(
			T[]	arr, 
			int	idx) {
		return removeAt(arr, idx, 1);
	}
	public static	<T>	T[]	removeAt(
			T[]	arr, 
			int	idx, 
			int	nbr) {
		int arrLen = arr.length;
		idx = limitRemoveIndex(idx, arrLen);
		nbr = limitNumber(idx, nbr, arrLen);
		T[] newArr = newArray((arrLen - nbr), arr);
		System.arraycopy(
				arr, 0, 
				newArr, 0, idx);
		System.arraycopy(
				arr, (idx + nbr), 
				newArr, idx, (arrLen - nbr - idx));
		return newArr;
	}
	// Shift methods
	public static	<T>	T[]	shift(
			T[]	arr, 
			T	val) {
		return shift(arr, newArray(arr, val));
	}
	public static	<T>	T[]	shift(
			T[]	arr, 
			T[]	vals) {
		return addAt(arr, vals, 0);
	}
	// Unshift methods
	public static	<T>	T[]	unshift(
			T[]	arr) {
		return unshift(arr, 1);
	}
	public static	<T>	T[]	unshift(
			T[]	arr, 
			int	nbr) {
		return removeAt(arr, 0, nbr);
	}
	// Trunc methods
	public static	<T>	T[]	trunc(
			T[]	arr) {
		return trunc(arr, 1);
	}
	public static	<T>	T[]	trunc(
			T[]	arr, 
			int	nbr) {
		return removeAt(arr, (arr.length - nbr), nbr);
	}
	// RemoveFrom methods
	public static	<T>	T[]	removeFrom(
			T[]	arr, 
			int	from) {
		return removeAt(arr, from, (arr.length - from));
	}
	// RemoveTo methods
	public static	<T>	T[]	removeTo(
			T[]	arr, 
			int	to) {
		return removeAt(arr, 0, (0 <= to ? to : arr.length + to));
	}
	// Slice methods
	public static	<T>	T[]	slice(
			T[]	arr, 
			int	idx, 
			int	nbr) {
		int arrLen = arr.length;
		idx = limitRemoveIndex(idx, arrLen);
		nbr = limitNumber(idx, nbr, arrLen);
		T[] newArr = newArray(nbr, arr);
		System.arraycopy(
				arr, idx, 
				newArr, 0, nbr);
		return newArr;
	}
	public static	<T>	T[]	slice(
			T[]	arr, 
			int	idx) {
		return slice(arr, idx, 1);
	}
}