#!/bin/bash

# Passed arguments
prod_project=$1
commit_message=$2
# Variables
module_name='org.twbillot.'"$prod_project" #org.twbillot.ArrayUtils
src_folder='src/'
jav_folder="$src_folder"'main/java/' #src/main/java/
bin_folder='bin/'
tar_folder='target/'
dep_folder='dependencies/'
# Update with master
git pull origin master
git rm -r "$prod_project/" #git rm -r ArrayUtils/
git add 'README.md' 'ToDo.txt' 'masterUpdate.sh'
git commit -m "Pre-update reset"
git push -u origin master
# Checkout dev
git checkout dev -- "$prod_project/$jav_folder" #git checkout dev -- ArrayUtils/src/main/java/
# Create src folder
mkdir -p "$src_folder" #mkdir -p src/
rm -rf "$src_folder"* #rm -rf src/*
mv "$prod_project/$src_folder" '.' #mv ArrayUtils/src/ .
# Create dependencies folder
mkdir -p "$dep_folder" #mkdir -p dependencies/
rm -rf "$dep_folder"* #rm -rf dependencies/*
# Compile the code
mkdir -p "$module_name/" #mkdir -p org.twbillot.ArrayUtils/
cp -r "$jav_folder"* "$module_name/" #cp -r src/main/java/* org.twbillot.ArrayUtils/
srcs='sources.txt'
find "$module_name/" -name "*.java" > $srcs #find org.twbillot.ArrayUtils/ -name "*.java" > sources.txt
javac -d '.' --module-source-path '.' --module "$module_name" "@$srcs" -Xlint:deprecation
#javac -d . --module-source-path . --module org.twbillot.ArrayUtils @sources.txt -Xlint:deprecation
# Create bin folder
mkdir -p "$bin_folder" #mkdir -p bin/
rm -rf "$bin_folder"* #rm -rf bin/*
cp -r "$module_name/"* "$bin_folder" #cp -r org.twbillot.ArrayUtils/* bin/
find "$bin_folder" -name "*.java" -type f -delete #find src/ -name "*.java" -type f -delete
# Create the library
cd "$module_name/" # cd org.twbillot.ArrayUtils/
bins='bins.txt'
find '.' -name "*.class" > $bins #find '.' -name "*.class" > bins.txt
jar -cf "$module_name.jar" "@$bins" #jar -cf org.twbillot.ArrayUtils.jar @bins.txt
cd ..
# Create target folder
mkdir -p "$tar_folder" #mkdir -p target/
rm -rf "$tar_folder"* #rm -rf target/*
mv "$module_name/$module_name.jar" "$tar_folder$module_name.jar"
#mv org.twbillot.ArrayUtils/org.twbillot.ArrayUtils.jar target/org.twbillot.ArrayUtils.jar
# Delete processing folders and files
rm -rf "$prod_project/" #rm -rf ArrayUtils/
rm -rf "$module_name/" #rm -rf org.twbillot.ArrayUtils/
rm $srcs #rm sources.txt
#rm $bins #rm bins.txt
# Push to master
git rm -r "$prod_project/" #git rm -r ArrayUtils/
git add "$src_folder" "$bin_folder" "$tar_folder" "$dep_folder" #git add src/ bin/ target/ dependencies/
git status
git commit -m "$commit_message" #git commit -m $2
git push -u origin master